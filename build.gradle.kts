import java.io.FileInputStream
import java.util.Properties

plugins {
    checkstyle
    jacoco
    signing
    `java-library`
    `java-library-distribution`
    `maven-publish`
    id("com.github.spotbugs") version "5.0.9"
    id("com.diffplug.spotless") version "6.8.0"
    id("io.github.gradle-nexus.publish-plugin") version "1.1.0"
    id("com.palantir.git-version") version "0.13.0"
}

// we handle cases without .git directory
val versionDetails: groovy.lang.Closure<com.palantir.gradle.gitversion.VersionDetails> by extra
val details = versionDetails()
val baseVersion = details.lastTag.substring(1)
if (details.isCleanTag) {  // release version
    version = baseVersion
} else {  // snapshot version
    version = baseVersion + "-" + details.commitDistance + "-" + details.gitHash + "-SNAPSHOT"
}

group = "tokyo.northside"

repositories {
    mavenCentral()
}

dependencies {
    implementation("io.github.dictzip:dictzip:0.13.0")
    implementation("com.github.takawitter:trie4j:0.9.8")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}

spotbugs {
    // excludeFilter.set(project.file("config/spotbugs/exclude.xml"))
    tasks.spotbugsMain {
        reports.create("html") {
            required.set(true)
        }
    }
    tasks.spotbugsTest {
        reports.create("html") {
            required.set(true)
        }
    }
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

jacoco {
    toolVersion="0.8.6"
}

tasks.jacocoTestReport {
    dependsOn(tasks.test) // tests are required to run before generating the report
    reports {
        xml.required.set(true) // coveralls plugin depends on xml format report
        html.required.set(true)
    }
}

java {
    withSourcesJar()
    withJavadocJar()
}

// we handle cases without .git directory
val home = System.getProperty("user.home")
val javaHome = System.getProperty("java.home")
val versionProperties = project.file("src/main/resources/version.properties")
val dotgit = project.file(".git")

if (dotgit.exists()) {
    apply(plugin = "com.palantir.git-version")
    val versionDetails: groovy.lang.Closure<com.palantir.gradle.gitversion.VersionDetails> by extra
    val details = versionDetails()
    val baseVersion = details.lastTag.substring(1)
    if (details.isCleanTag) {  // release version
        version = baseVersion
    } else {  // snapshot version
        version = baseVersion + "-" + details.commitDistance + "-" + details.gitHash + "-SNAPSHOT"
    }
    publishing {
        publications {
            create<MavenPublication>("mavenJava") {
                from(components["java"])
                pom {
                    name.set("stardict4j-light")
                    description.set("Stardict access library for java")
                    url.set("https://codeberg.org/miurahr/stardict4j-light")
                    licenses {
                        license {
                            name.set("The GNU General Public License, Version 3")
                            url.set("https://www.gnu.org/licenses/licenses/gpl-3.html")
                            distribution.set("repo")
                        }
                    }
                    developers {
                        developer {
                            id.set("miurahr")
                            name.set("Hiroshi Miura")
                            email.set("miurahr@linux.com")
                        }
                    }
                    scm {
                        connection.set("scm:git:git://codeberg.org/miurahr/stardict4j-light.git")
                        developerConnection.set("scm:git:git://codeberg.org/miurahr/stardict4j-light.git")
                        url.set("https://codeberg.org/miurahr/stardict4j-light")
                    }
                }
            }
        }
    }

    val signKey = listOf("signingKey", "signing.keyId", "signing.gnupg.keyName").find {project.hasProperty(it)}
    tasks.withType<Sign> {
        onlyIf { details.isCleanTag && (signKey != null) }
    }

    signing {
        when (signKey) {
            "signingKey" -> {
                val signingKey: String? by project
                val signingPassword: String? by project
                useInMemoryPgpKeys(signingKey, signingPassword)
            }
            "signing.keyId" -> { /* do nothing */ }
            "signing.gnupg.keyName" -> { useGpgCmd() }
        }
        sign(publishing.publications["mavenJava"])
    }

    nexusPublishing {
        repositories{
            sonatype()
        }
    }
} else if (versionProperties.exists()) {
    version = Properties().apply { load(FileInputStream(versionProperties)) }.getProperty("version")
}

tasks.register("writeVersionFile") {
    val folder = project.file("src/main/resources")
    if (!folder.exists()) {
        folder.mkdirs()
    }
    versionProperties.delete()
    versionProperties.appendText("version=" + project.version)
}

tasks.getByName("jar") {
    dependsOn("writeVersionFile")
}
